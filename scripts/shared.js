// Description:
//   Handle shared information between states/handlers
//
// Notes:
//   Hi! Yes!
//
// Author:
//   Josh Beckman <email@andjosh.com>


module.exports = function(robot) {
    robot.hear(/^[yes|yeah|sure]/i, function(res) {
        var user,
            confirming,
            staged;

        user = res.envelope.user;
        confirming = user.confirming;
        staged = user.staged;
        if (staged && confirming) {
            robot.brain.data.user[confirming] = staged;
            res.send('Got it! Confirmed your ' + confirming.split('_').join(' '));
        }
    });
};
