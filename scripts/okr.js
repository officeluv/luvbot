// Description:
//   Provides an interface for tracking and updating Objectives and Key Results
//
// Commands:
//   hubot okr - Respons with the current OKR and status
//   hubot set okr objective to <objective sentence> - Set/Update Objective
//   hubot set okr key result [n] to <Key result> - Set/Update nth Key Result
//   hubot set okr key result [n] confidence to <number> - Set/Update nth Key Result confidence level to <number>/10
//   hubot remove okr key result [n] - Remove nth Key Result
//
// Notes:
//   Format:
//      Objective: <objective>
//      Key Result: # <metric>
//      Key Result: # <metric>
//
// Author:
//   Josh Beckman (andjosh) <email@andjosh.com>

var CURRENT_OKR = 'current_okr',
    OKR_SCHEMA;

OKR_SCHEMA = {
    objective: '',
    results: [
        {
            result: 'Set your metric-based result',
            confidence: 0.5
        }
    ]
};

module.exports = function(robot) {
    robot.respond(/okr$/i, function(res) {
        var okr,
            keyResults;

        okr = robot.brain.data[CURRENT_OKR];
        if (okr) {
            keyResults = formatKeyResults(okr);
            res.send(
                'Objective: ' + okr.objective + '\n'
                + keyResults
            );
        } else {
            replyNoOKR(res);
        }
    });

    robot.respond(/set okr objective( to)? (.*)/i, function(res) {
        var okr,
            keyResults;

        okr = robot.brain.data[CURRENT_OKR] || OKR_SCHEMA;
        okr.objective = res.match[2];
        robot.brain.data[CURRENT_OKR] = okr;
        res.send(
            'Objective set: ' + okr.objective
        );
    });

    robot.respond(/set okr key result ([1-9])( to)? (.*)/i, function(res) {
        var okr,
            num = 0,
            result = '',
            keyResults;

        num = (parseInt(res.match[1], 10) - 1) || 0;
        result = res.match[3];
        okr = robot.brain.data[CURRENT_OKR];
        if (okr) {
            okr.results.splice(num, 1, {result: result, confidence: 0.5});
            robot.brain.data[CURRENT_OKR] = okr;
            res.send(
                'Key Result ' + (num + 1) + ' set: ' + result
            );
        } else {
            replyNoOKR(res);
        }
    });

    robot.respond(/set okr key result ([1-9]) confidence( to)? (.*)/i, function(res) {
        var okr,
            num = 0,
            confidence = 5,
            keyResults;

        num = (parseInt(res.match[1], 10) - 1) || 0;
        confidence = (parseInt(res.match[3], 10) - 1) || 5;
        okr = robot.brain.data[CURRENT_OK];
        if (okr) {
            if (okr.results[num]) {
                okr.results[num].confidence = confidence / 10;
            }
            robot.brain.data[CURRENT_OKR] = okr;
            res.send(
                'Key Result ' + (num + 1) + ' confidence set: ' + result
            );
        } else {
            replyNoOKR(res);
        }
    });

    robot.respond(/remove okr key result ([1-9])/i, function(res) {
        var okr,
            num = 0,
            result = ''
            keyResults;

        num = (parseInt(res.match[1], 10) - 1) || 0;
        okr = robot.brain.data[CURRENT_OKR];
        if (okr) {
            okr.results.splice(num, 1);
            robot.brain.data[CURRENT_OKR] = okr;
            res.send(
                'Key Result ' + (num + 1) + ' removed'
            );
        } else {
            replyNoOKR(res);
        }
    });

    function replyNoOKR(res) {
        res.send('No OKR has been set, yet. Ask me how with "@<me> help okr"');
    }

    function formatKeyResults(okr) {
        var results,
            confidence = 0,
            response = '';

        results = okr.key_results;
        for (var i in results) {
            confidence = results[i].confidence;
            response += 'Key Result: ' + results[i].result
                + ' - Confidence: ' + (confidence * 10) + '/10\n';
        }

        return response;
    }
};
